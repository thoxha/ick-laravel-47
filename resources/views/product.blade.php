@extends('layouts/baza')

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@endsection

@section('content')

    <div class="col-2">
        @if ($product->product_image == '/products/no-image.png')
            <img src="{{ $product->product_thumbnail }}" class="img-fluid">
        @else
            <a data-fancybox="gallery" href="{{ $product->product_image }}">
                <img src="{{ $product->product_thumbnail }}" class="img-fluid">
            </a>
        @endif
    </div>
    <table class="table">
        <tr>
            <th>Emri</th>
            <td><h1>{{ $product->product_name  }}</h1></td>
        </tr>
        <tr>
            <th>Barkodi</th>
            <td>{{ $product->barcode }}</td>
        </tr>
        <tr>
            <th>Cmimi</th>
            <td>{{ $product->price }}</td>
        </tr>
        <tr>
            <th>Data</th>
            <td>{{ $product->created_at }}</td>
        </tr>

        <tr>
            <td>Pershkrimi</td>
            <td>@parsedown($product->description)</td>
        </tr>
    </table>
@endsection

@section('javascript')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endsection
