<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>{{ $student->student_name }}</h1>

<form method="post" action="{{ route('students.attach') }}">
    @csrf
    <input type="hidden" name="student_id" value="{{ $student->id }}">
    <select name="subject_id">
        @foreach($subjects as $subject)
            <option value="{{ $subject->id }}">{{ $subject->subject_name }}</option>
        @endforeach
    </select>
    <p>Nota: <input type="number" name="grade"></p>
    <input type="submit" value="REGJISTRO">
</form>

<table cellpadding="4" cellspacing="4" border="1">
    @foreach ($student->subjects as $subject)
        <tr>
            <td>{{ $subject->subject_name }}</td>
            <td>{{ $subject->pivot->grade }}</td>
            <td><a href="{{ route('students.detach', ['student' => $student, 'subject' => $subject]) }}">Detach</a></td>
        </tr>
    @endforeach
</table>
</body>
</html>
