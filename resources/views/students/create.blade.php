<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>Regjistri i nje studenti</h1>

<form method="POST" action="/students/store">
    @csrf
    <label for="emri_mbiemri">Emri dhe mbiemri:</label>
    <input type="text" id="emri_mbiemri" name="emri_mbiemri">
    <label for="qyteti">Qyteti:</label>
    <input type="text" id="qyteti" name="qyteti">

    <input type="submit" value="REGJISTRO">
</form>

</body>
</html>
