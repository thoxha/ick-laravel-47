Konfirmimi i porosise

Klienti: {{ $clientName }}

Konfirmojme se me daten {{ $orderDate }} keni bere porosi me permbajtje vijuese:
<table style="border: 2px solid black">
    @foreach($orderItems as $item)
        <tr>
            <td style="border:1px solid gray; padding: 6px">{{ $item['name'] }}</td>
            <td style="border:1px solid gray; padding: 6px">{{ $item['quantity'] }}</td>
            <td style="border:1px solid gray; padding: 6px">{{ $item['price'] }}</td>
            <td style="border:1px solid gray; padding: 6px">{{  $item['quantity'] * $item['price'] }}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{ $orderTotal }}</td>
    </tr>
</table>
