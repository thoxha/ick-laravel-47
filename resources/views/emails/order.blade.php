<img style="text-align: center; width: 150px" src="http://acb9297fc5f7.ngrok.io/images/logo.png">
<h1 style="color:blue">Konfirmimi i porosise</h1>

<img src="{{ $message->embed('images/logo.png') }}">



<h2>Klienti: {{ $clientName }}</h2>
<img src="{{ $message->embedData($imageData, 'fotografia.png') }}">

<p>Konfirmojme se me daten {{ $orderDate }} keni bere porosi me permbajtje vijuese:</p>
<table style="border: 2px solid black">
    @foreach($orderItems as $item)
        <tr>
            <td style="border:1px solid gray; padding: 6px">{{ $item['name'] }}</td>
            <td style="border:1px solid gray; padding: 6px">{{ $item['quantity'] }}</td>
            <td style="border:1px solid gray; padding: 6px">{{ $item['price'] }}</td>
            <td style="border:1px solid gray; padding: 6px">{{  $item['quantity'] * $item['price'] }}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{ $orderTotal }}</td>
    </tr>
</table>
