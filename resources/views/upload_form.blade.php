@extends('layouts.baza')


@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('files.submit_form') }}" enctype="multipart/form-data">
        @csrf

        <div>
            <label for="emri">Sheno emrin e produktit:</label>
            <input type="text" name="product_name" id="emri" value="{{ old('product_name', '') }}" class="form-control">
        </div>

        <div>
            <label for="barcode">Sheno barkodin e produktit:</label>
            <input type="text" name="barcode" id="barcode" value="{{ old('barcode', '') }}" class="form-control">
        </div>
        <div>
            <label for="category_id">Kategoria:</label>
            <select id="category_id" name="category_id">
                @foreach($categories as $category)

                    <option
                        @if ($category->id == old('category_id'))
                        selected
                        @endif
                        value="{{ $category->id }}">{{ $category->category_name }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <label for="description">Pershkrimi:</label>
            <textarea id="description" name="description" rows="5"
                      class="form-control">{{ old('description') }}</textarea>
        </div>

        <div>
            <label for="price">Cmimi:</label>
            <input type="text" name="price" id="price" value="{{ old('price', 0) }}" class="form-control">
        </div>
        <div>
            <input type="file" name="profile_picture">
        </div>
        <input type="submit" value="DERGO">

    </form>
@endsection
