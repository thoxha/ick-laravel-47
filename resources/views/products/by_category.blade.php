@extends('layouts.baza')

@section('content')
<h1>Category: {{ $category->category_name }}</h1>

@include('products/partials/list')

@endsection

