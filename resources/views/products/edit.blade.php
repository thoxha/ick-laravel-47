@extends('layouts.baza')

@section('content')
<div style="background-color: #ffb597">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif
</div>
<form method="post" action="{{ route('products_update') }}">
    @csrf
    <input type="hidden" name="product_id" value="{{ $product->id }}">
    <p>Emri: <input class="form-control" type="text" name="product_name" value="{{ old('product_name', $product->product_name) }}"></p>
    <p>Barkodi: <input class="form-control" type="text" name="barcode" value="{{ old('barcode', $product->barcode) }}" maxlength="13" minlength="13"></p>
    <p>Price: <input class="form-control" type="text" name="price" value="{{ old('price', $product->price) }}"></p>


    <div>
        <select name="category_id" class="form-select" >
            <option value="0">Zgjedhe kategorine:</option>
            @foreach($categories as $category)
                <option
                    @if ($category->id == old('category_id', $product->category_id)) selected @endif
                value="{{ $category->id }}">{{ $category->category_name }}</option>
            @endforeach
        </select>
    </div>

    <input type="submit" class="btn btn-primary" value="REGJISTRO">
</form>
@endsection
