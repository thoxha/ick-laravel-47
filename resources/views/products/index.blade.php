@extends('layouts.baza')

@section('content')
<a href="{{ route('products_create') }}">Add new product</a>
@include('products/partials/list')

    {{ $products->links() }}

@endsection

@section('sidebar')

    <h4>Sidebar</h4>
@endsection
