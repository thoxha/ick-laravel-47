<table class="table">
    <tr>
        <th>edit</th>
        <th>Emertimi</th>
        <th>Barkodi</th>
        <th>Cmimi</th>
        <th>Shfaqe</th>
        <th>Fshije</th>
    </tr>
    @foreach($products as $product)
        <tr
            @if ($loop->even)
            style="background-color: #edf2f7"
            @endif
        >
            <td><a href="{{ route('products_edit', ['id' => $product->id]) }}">Edito</a></td>
            <td>{!!  $product->product_name !!}</td>
            <td>{{ $product->barcode }}</td>
            <td>{{ $product->price }}</td>
            <td><a href="{{ route('products_show', [$product]) }}">Shfaqe</a></td>
            <td><a href="{{ route('products_destroy', ['id' => $product->id]) }}">Fshije</a></td>
        </tr>
    @endforeach
</table>
