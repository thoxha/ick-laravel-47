@extends('layouts/baza')

@section('content')
    <table class="table">
        <tr>
            <th>Emri</th>
            <td><h1>{{ $product->product_name  }}</h1></td>
        </tr>
        <tr>
            <th>Barkodi</th>
            <td>{{ $product->barcode }}</td>
        </tr>
        <tr>
            <th>Cmimi</th>
            <td>{{ $product->price }}</td>
        </tr>
        <tr>
            <th>Data</th>
            <td>{{ $product->created_at }}</td>
        </tr>

        <tr>
            <td>Pershkrimi</td>
            <td>@parsedown($product->description)</td>
        </tr>
    </table>

    @json($lista)

    @php
        echo date("d.m.Y", time());
    @endphp
@endsection
