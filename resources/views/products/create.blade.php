@extends('layouts.baza')

@section('content')
<div style="background-color: #ffb597">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif
</div>
<form method="post" action="{{ route('products_store') }}">
    @csrf
    <p>Emri: <input type="text" name="product_name" value="{{ old('product_name') }}"></p>
    <p>Barkodi: <input type="text" name="barcode" value="{{ old('barcode') }}" maxlength="13" minlength="13"></p>
    <p>Price: <input type="text" name="price" value="{{ old('price') }}"></p>

    <div>
        <p>Description</p>
        <textarea name="description" class="form-control" rows="10">{{ old('description') }}</textarea>
    </div>
    <div>
        <select name="category_id">
            <option value="0">Zgjedhe kategorine:</option>
            @foreach($categories as $category)
                <option
                    @if ($category->id == old('category_id')) selected @endif
                    value="{{ $category->id }}">{{ $category->category_name }}</option>
            @endforeach
        </select>
    </div>

    <input type="submit" value="REGJISTRO">
</form>
@endsection
