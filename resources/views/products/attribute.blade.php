@extends('layouts.baza')

@section('content')
    <h1>{{ $product->product_name }}</h1>
    <p>Normal price: {{ $product->price  }}</p>
    <p>With VAT: {{ $product->price_with_vat }}</p>
    <p>{{ $product->new_price }}</p>
    <p>Total: {{ $product->total($qty) }}</p>

@endsection
