@extends('layouts.baza')


@section('content')

    @foreach(config('custom.languages') as $locale )
        @if ($locale != App::currentLocale())
        <p><a href="{{ route('translations.details', ['lang' => $locale, 'id' => $product->id]) }}">{{ $locale }}</a>
        </p>
        @endif
    @endforeach
    <h1>@lang('messages.detajet_produkti')</h1>
    <h2>{{ $product->product_name }}</h2>
@endsection
