@extends('layouts.baza')


@section('content')

    <p>{{ trans_choice('messages.apples', 1) }}</p>
    <p>{{ trans_choice('messages.minutes_ago', 5, ['value' => 5]) }}</p>

    <h1>@lang('messages.greeting', ['name' => $name])</h1>
    <h1>@lang('messages.lista_produkteve')</h1>
    <h1>{{ __('messages.lista_produkteve')  }}</h1>
    <h2>{{ __('List of the most popular products') }}</h2>
    @foreach($products as $product)
        <p><a href="{{ route('translations.details', ['lang' => App::currentLocale(), 'id' => $product->id]) }}">{{ $product->product_name }}</a></p>

    @endforeach
@endsection
