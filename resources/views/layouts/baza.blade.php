<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@if (isset($product_title))
            {{ $product_title }}
        @else
            MyApp
        @endif</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    @yield('head')
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12"><img src="/images/banner.png" class="w-100"></div>
        @include('layouts/partials/menu')

        <main class="row">
            <article class="col-md-8">
                @yield('content')
            </article>
            <aside class="col-md-4">
                @yield('sidebar')
            </aside>
        </main>

        <footer class="row">
            <div class="col-12">
                <p>Innovation Centre Kosova 2021</p>
                <p>Total products: {{ $total_products }}</p>
            </div>
        </footer>
    </div>

    @include('layouts/partials/ga')
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>

@yield('javascript')
</body>
</html>
