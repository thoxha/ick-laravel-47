<?php


return [
    'produkti_ne_shporte' => 'Produkti u shtua në shportë me sukses!',
    'produkti_fshirja' => 'Produkti u largua!',
    'lista_produkteve' => 'Lista e produkteve',
    'detajet_produkti' => 'Detajet e produktit',
    'greeting' => "Tung :name",
    'minutes_ago' => '{1} para :value minuti|[2,*] para :value minutash',
];
