<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Kredencialet nuk përputhem me shënimet në databazë.',
    'password' => 'Fjalëkalimi nuk është i saktë.',
    'throttle' => 'Shumë tentime për identifikim. Provoni prapë pas :seconds sekondave.',

];
