<?php


return [
    'produkti_ne_shporte' => 'The product has been added to shopping cart!',
    'produkti_fshirja' => 'The product has been removed!',
    'lista_produkteve' => 'Products list',
    'detajet_produkti' => 'Product details',
    'greeting' => "Hello :name",
    'apples' => "There is one apple|There are many apples",
];
