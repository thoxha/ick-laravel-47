<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = new Teacher();

        $teacher->name = "Tahir Hoxha";
        $teacher->email = "tahir@codemolecules.com";
        $teacher->password = Hash::make("12345678");
        $teacher->save();
    }
}
