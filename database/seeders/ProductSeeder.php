<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $product = new Product();
        $product->product_name = "Cokolade 100g";
        $product->barcode = rand(1000000000000, 9999999999999);
        $product->description = "Lorem ipsum...";
        $product->category_id = 3;
        $product->price = 4.50;
        $product->save();
    }
}
