<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = collect([
            ['name' => "Tahir Hoxha", 'email' => "tahir.hoxha@gmail.com", 'password' => Hash::make("12345678")],
            ['name' => "Ylli Gorani", 'email' => "yll.gorani@gmail.com", 'password' => Hash::make("12345678")],

        ]);

        $users->map(function ($user, $key){
            DB::table('users')->insert($user);
        });


    }
}
