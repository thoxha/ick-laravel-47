<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $category = new Category();
       $category->category_name = "Peme dhe perime";
       $category->save();

        $category = new Category();
        $category->category_name = "Mish";
        $category->save();
    }
}
