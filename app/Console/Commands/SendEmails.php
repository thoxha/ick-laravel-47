<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send {user?} {--queue} {--opc1} {--opc2}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends emails to subscribers of the newsletter.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        $bar = $this->output->createProgressBar(count($users));

        $bar->start();

        foreach ($users as $user) {
           sleep(1);

            $bar->advance();
        }

        $bar->finish();

        die();

        $users = $this->withProgressBar(User::all(), function ($user) {
            sleep(1);
        });


        die();

        $this->table(
            ['Name', 'Role'],
            User::all(['name', 'role'])->toArray()
        );

        die();
        $name = $this->choice(
            'What is your name?',
            ['Taylor', 'Dayle'],
            0
        );

        $this->comment($name);
        die();
        $name = $this->anticipate('What is your name?', function ($input) {
            $users = User::where('name', 'like', $input.'%')->orderBy('name', 'asc')->get();
            return $users;
        });

        $this->comment("Your name: ".$name);

        die();


        $name = $this->ask('What is your name?');

        $city = $this->anticipate('City?', ['Mitrovice', 'Prizren', 'Prishtine']);
        $this->comment($city);




        $password = $this->secret('What is the password?');

        if ($this->confirm('Ky operacion do ti fshije te dhenat. A je i sigurte?', false)) {
            $this->error("U fshi tabela!");
        }

        $userid =  $this->argument('user');


        if($this->option('opc2')) {
            $this->line('Eshte zgjedhur opcioni 2');
        }
        $this->info($userid);
        if (empty($userid)) {
            $userid = 1;
        }

        $this->error($userid);
//        $format  = $this->argument('format');
        $user = User::find($userid);
        $this->comment($user->email);
        return 0;
    }
}
