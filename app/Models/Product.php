<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $hidden = [
        'secret'
    ];

//    public function getRouteKeyName()
//    {
//        return 'product_slug';
//    }


    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


    public function getPriceWithVatAttribute()
    {
        return $this->price * 1.15;
    }

    public function getPriceWithVatCurrencyAttribute()
    {
        return $this->price_with_vat . config('custom.default_currency');;
    }

    public function getNewPriceAttribute()
    {
        return "New Discounted Price: " . $this->price_with_vat_currency;
    }

    public function total($t = 1)
    {
        return $this->price_with_vat * $t;
    }

    public function setNewPriceAttribute($value)
    {
        $value = (float)filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $this->attributes['price'] = $value * 1.15;
    }

    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = $value['price'] * $value['quantity'] * 1.15;
    }

    public static function calculate($price, $qty)
    {
        return $price * $qty * 1.15;
    }

    public function getProductThumbnailAttribute()
    {
        $filename1 = "products/thumbnails/foto-" . $this->id . ".jpg";
        $filename2 = "products/thumbnails/foto-" . $this->id . ".png";
        if (Storage::exists($filename1)) {
            return "/" . $filename1;
        } elseif (Storage::exists($filename2)) {
            return "/" . $filename2;
        } else {
            return '/products/no-image.png';
        }

    }


    public function getProductImageAttribute()
    {

        $filename1 = "products/foto-" . $this->id . ".jpg";
        $filename2 = "products/foto-" . $this->id . ".png";
        if (Storage::exists($filename1)) {
            return "/" . $filename1;
        } elseif (Storage::exists($filename2)) {
            return "/" . $filename2;
        } else {
            return '/products/no-image.png';
        }

    }
}
