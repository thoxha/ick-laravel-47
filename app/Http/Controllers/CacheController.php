<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class CacheController extends Controller
{

    public function query() {
        $products =  Cache::remember('variabli',now()->addMinutes(1), function() {
            sleep(5);
            $p= Product::all();
            return $p;
        });
        foreach($products as $product) {
            echo "<p>".$product->product_name;
        }
    }


    public function lexoje() {
       $v =  Cache::remember('variabli',now()->addMinutes(1), function() {
           sleep(5);
           return rand(1000, 9999);
       });
       dd($v);
    }

    public function ruaje()
    {
        Cache::put('variabli', rand(1000, 9999), now()->addMinutes(1));
//        cache(['variabli' => rand(1000, 9999)], now()->addMinutes(1);
    }


    public function mirembajtja($id) {
        $exitCode = Artisan::call('emails:send', [
            'user' => $id, '--opc1'
        ]);

        if ($exitCode == 0) {
            echo "Komanda u ekzekutua me sukses";
        }
    }
}
