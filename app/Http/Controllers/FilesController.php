<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FilesController extends Controller
{


    public function ruaje()
    {
        $content = "Poezia ‘Bagëti e bujqësi’ nga Naim Frashëri.

O malet’ e Shqipërisë e ju o lisat’ e gjatë!
Fushat e gjëra me lule, q’u kam ndër mënt dit’ e natë!
Ju bregore bukuroshe e ju lumenjt’ e kulluar!
Çuka, kodra, brinja, gërxhe dhe pylle të gjelbëruar!
Do të këndonj bagëtinë që mbani ju e ushqeni,
O vendëthit e bekuar, ju mëndjen ma dëfreni.

Ti Shqipëri, më ep nderë, më ep emrin shqipëtar,
Zëmrën ti ma gatove plot me dëshirë dhe me zjarr.

Shqipëri, o mëma ime, ndonëse jam i mërguar,
Dashurinë tënde kurrë zemëra s’e ka harruar.";

        Storage::put('fajlli.txt', $content);
    }

    public function lexoje()
    {
        $permbajtja = Storage::get('fajlli.txt');

        echo nl2br($permbajtja);
    }


    public function db2csv()
    {
        $users = User::orderBy('name')->get();
        $csv = "";
        foreach ($users as $user) {
            $csv .= $user->name . ", " . $user->email . ", " . $user->role . "\n";
        }

        Storage::disk('products')->put('lista.csv', $csv);
//        Storage::disk('ftp')->put('lista.csv', $csv);
    }


    public function linku()
    {
//        echo "<a href='/products/8690572412275.jpg'>Shkarko</a>";
//        echo "<br><a href='". asset('products/8690572412275.jpg')."'>Shkarko</a>";

        return Storage::disk('products')->download('8690572412275.jpg', 'fotografia.jpg', ["Content-type: image/jpeg"]);

    }

    public function url()
    {
        $url = Storage::disk('products')->url('8690572412275.jpg');
        echo $url . "<br>";
        $path = Storage::disk('products')->path('8690572412275.jpg');
        echo $path . "<br>";
        echo Storage::disk('products')->size('8690572412275.jpg');
        $ts = Storage::disk('products')->lastModified('8690572412275.jpg');
        echo "<br>" . date("d.m.Y H:i:s", $ts);

    }

    public function kopjo()
    {
        Storage::copy('products/lista.csv', 'public/lista2.csv');
    }

    public function form()
    {
        $categories = Category::orderBy('category_name')->get();
        return view('upload_form', compact('categories'));
    }

    public function submitForm(Request $request)
    {
        $request->validate([
            'product_name' => 'required|string|min:5|max:100',
            'barcode' => 'required|string|min:9|max:13',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|min:0',
            'profile_picture' => 'mimes:jpg,png|dimensions:min_width=600,min_height=600,max_width=2000,max_height=2000'
        ]);


        $emri = $request->input('emri');

        $pp = $request->file('profile_picture');
        if ($pp !== null) {
//            $file_name = $request->file('profile_picture')->store('products');

            $product = new Product();
            $product->product_name = filter_var($request->input('emri'), FILTER_SANITIZE_STRING);
            $product->barcode = filter_var($request->input('barcode'), FILTER_SANITIZE_STRING);
            $product->category_id = filter_var($request->input('category_id'), FILTER_SANITIZE_NUMBER_INT);
            $product->description = filter_var($request->input('description'), FILTER_SANITIZE_STRING);
            $product->price = filter_var($request->input('price'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $product->user_id = Auth::id();
            $product->save();

            $extension = $request->file('profile_picture')->extension();

            $file_name = $request->file('profile_picture')->storeAs(
                'products', "foto-" . $product->id . "." . $extension
            );

//            $file_name = Storage::putFile('products', $request->file('profile_picture'));


            $img = Image::make($file_name);
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            // Fotoja  800px
            $img->save($file_name);

            $img2 = Image::make($file_name);
            $img2->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $thumbnail = 'products/thumbnails/foto-' . $product->id . '.' . $extension;
            $img2->save($thumbnail);

            echo $thumbnail;

        } else {
            echo "Nuk keni zgjedhur asnje fajll";
        }
    }


    public function product($id)
    {
        $product = Product::findOrFail($id);
        return view('product', compact('product'));
    }

    public function delete($id)
    {

        $product = Product::find($id);
        $product->delete();

        $filename1 = "products/foto-" . $id . ".jpg";
        $filename1thumb = "products/thumbnails/foto-" . $id . ".jpg";
        $filename2 = "products/foto-" . $id . ".png";
        $filename2thumb = "products/thumbnails/foto-" . $id . ".png";

        if (Storage::exists($filename1)) {
            Storage::delete($filename1);
            echo "<p>Fshirje me sukses! ".$filename1;
            if (Storage::exists($filename1thumb)) {
                Storage::delete($filename1thumb);
                echo "<p>Fshirje me sukses! ".$filename1thumb;
            }


        } elseif (Storage::exists($filename2)) {
            Storage::delete($filename2);
            echo "<p>Fshirje me sukses! ".$filename2;
            if (Storage::exists($filename2thumb)) {
                Storage::delete($filename2thumb);
                echo "<p>Fshirje me sukses! ".$filename2thumb;
            }
        } else {
            echo "Nuk ka foto per kete produkt";
        }

    }
}
