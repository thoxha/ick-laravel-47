<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Country;
use App\Models\Product;
use App\Models\Student;
use App\Models\StudentSubject;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentsController extends Controller
{
//    public function __construct() {
//        $this->middleware('admin');
//    }

    public function db()
    {
//
//        $users = DB::table('users')->where('status', '1')
//            ->where('subscribed', '<>', '1')->get();
//

        $status = 1;
        $subscribed = 2;
        $arraySql = [];

        if ($status > 0) {
           $arraySql[] = ['status', '=', $status];
        }

        if ($subscribed > 0) {
            $arraySql[] = ['subscribed', '=', '1'];
        }

        $users = DB::table('users')->where($arraySql)->toSql();
        dd($users);

        die();

        $first = DB::table('students')
            ->whereNull('student_name');

        $users = DB::table('students')
            ->whereNull('student_surname')
            ->union($first)
            ->get();

        dd($users);
        die();

        $products = DB::table('categories')
            ->rightJoin('products', 'categories.id', '=', 'products.category_id')
            ->get();

        dd($products);

        die();
        $users = DB::table('students')
            ->select(DB::raw('count(*) as student_count, status'))
            ->groupBy('status')
            ->toSql();
dd($users);
      foreach($users as $user) {
          echo "<p>".$user->status. " : ".$user->student_count. "</p>";
      }

        die();
        $manufacturer_id = 10;
        $price = 5000;
        $year  = 2015;

        $cars = DB::table('cars')->orderBy('car_year', 'desc');

        if ($manufacturer_id > 0) {
            $cars->where('manufacturer_id', $manufacturer_id);
        }

        if ($price > 0) {
            $cars->where('car_price', '>', $price);
        }

        if ($year > 0) {
            $cars->where('car_year', '>=', $year);
        }

        echo $cars->toSql();

        die();
        $x =5;

        $products = DB::table('products')->select('product_name')->orderBy('product_name');
        if ($x == 5) {
            $products->addSelect('price');
            $products->where('price', '>', 1);
        }
        dd($products->toSql());

        die();

        $users = DB::table('students')->select('qyteti')->distinct()->get();
        dd($users);
        die();


        if (DB::table('products')->where('id', '>',5)->exists()) {
            //
        }

        die();


        $product = Product::find(3);
        $product->price = 2.56;
        $product->save();

        DB::table('products')
            ->where('id', 3)
            ->update(['price' => 2.56]);


        DB::table('users')->where('active', false)
            ->chunkById(100, function ($users) {
                foreach ($users as $user) {
                    DB::table('users')
                        ->where('id', $user->id)
                        ->update(['active' => true]);
                }
            });


        die();
        DB::table('products')->orderBy('id')->chunk(2, function ($products) {
            foreach($products as $product) {
                echo "<p>".$product->product_name."</p>";
            }
            echo "<hr>";

        });

        die();


        $products = Product::orderBy('product_name')->pluck('id','price');
        foreach ($products as $price=>$id) {
            echo "<p>".$id." -> ".$price;
        }


        die();
        dd($products);
        foreach ($products as $product) {
            dump($product);
            echo "<p>" . $product . "</p>";
        }
        echo "<hr>";

        $products = DB::table('products')
            ->orderBy('product_name')->pluck('price');
        foreach ($products as $product) {
            echo "<p>" . $product . "</p>";
        }

        die();
        $products = DB::table('products')->get();

        $product = Product::find($products[0]->id);
        dd($product->category->category_name);
        dd($products);
    }

    public function test()
    {

        $products = DB::select('select * from products inner join categories

    on categories.id = products.category_id');
        foreach ($products as $product) {
            echo "<p>" . $product->product_name . "</p>";
        }
        echo "<hr>";

        $products = Product::all();
        foreach ($products as $product) {
            echo "<p>" . $product->category->category_name . "</p>";
        }

        die();

        DB::beginTransaction();

        DB::insert('insert into products set product_name=:pn, barcode=:bc,
           price=:pc, category_id=:cid, product_slug=:ps, user_id=:uid',
            ['pn' => "ABC", 'ps' => "abc", 'uid' => 1,
                'bc' => "1233333333333", 'pc' => 5, 'cid' => 3]);

        $pid = DB::getPdo()->lastInsertId();

        $result = DB::update('update students set product_id = :pid where id = :id', ['pid' => $pid, 'id' => 2]);
        if (!$result) {
            DB::rollBack();
        } else {
            DB::commit();
        }


//        DB::transaction(function () {
//
//
//
//
//        });

        die();

        $a = 5;
        $products = DB::select('select * from products where id = :id AND price > :price', ['id' => 2, 'price' => 5]);
        dd($products);


        die();
        $product = Product::orderBy('price')
            ->where('id', '>', 5)->first();
        dd($product->product_name);


        die();
        $lista = collect([
            1, 6, 9, 11, 89
        ]);

        $students = User::all();
        $student = User::find(1);

        $ekziston = $students->contains($student);

        dd($ekziston);


        echo $lista->count();
        echo "<br>";
        echo $lista->min();
        echo "<br>";
        echo $lista->max();
        echo "<br>";
        echo $lista->average();

        echo "<br>";

        $names = Product::all()->reject(function ($product) {
            return $product->price < 10;
        })->map(function ($product) {
            return $product->product_name;
        });

        dd($names);
    }

    public function countryVillages(Country $country)
    {

        return view('country_villages', compact('country'));

    }

    public function notat(Student $student)
    {

        $subjects = Subject::orderBy('subject_name')->get();
        return view('students/notat', compact('student', 'subjects'));
    }


    public function attach(Request $request)
    {

        $request->validate([
            'student_id' => 'required|exists:students,id',
            'subject_id' => 'required|exists:subjects,id',
            'grade' => 'required|integer|min:1|max:5'
        ]);

        $student_id = (int)$request->input('student_id');
        $subject_id = (int)$request->input('subject_id');
        $grade = (int)$request->input('grade');

        $student = Student::find($student_id);

        $student->subjects()->attach($subject_id, ['grade' => $grade]);

        return redirect(route('students.notat', [$student]));

//    if ($rel !== null) {
//        $student->subjects()->detach($subject->id);
//    }
//        $subject->students()->detach($student->id);
    }


    public function detach(Student $student, Subject $subject)
    {
        $student->subjects()->detach($subject->id);
        return redirect(route('students.notat', ['student' => $student]));
    }


    public function index()
    {

        return view('students/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shteti = "Kosova";
        $output['emri_mbiemri'] = $request->input('emri_mbiemri');
        $output['qyteti'] = $request->input('qyteti');


//        return view('students/store', compact('emri_mbiemri', 'qyteti'));

//        return view('students/store',
//            ['emri_mbiemri' => $emri_mbiemri,'qyteti' => $qyteti]);

        return view('students/store', $output);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function authenticate(Request $request)
    {
//        Auth::login(User::find(5));
//        Auth::loginUsingId(5);


        if (Auth::guard('teachers')->attempt(['email' => "tahir@codemolecules.com", 'password' => "12345678"], true)) {
            $request->session()->regenerate();
            echo "Jeni autentikuar si: ";
            echo Auth::guard('teachers')->user()->name;
//            return redirect()->intended('dashboard');
        } else {
            echo "Gabim ne autentikim";
        }


    }

    public function logout(Request $request)
    {

//        Auth::logoutOtherDevices('12345678');

        Auth::guard('teachers')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
