<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function read() {
//        dd(session('shopping_cart'));
        $data = session()->all();
        dd($data);
    }

    public function push() {
       session()->push('user.numbers', rand(1000,9999));
    }

    public function pull() {
        $value = session()->pull('shopping_cart', 'default');
        dump($value);

        dd(session('shopping_cart'));

    }


    public function inc() {
        session()->increment('count', 5);

    }

    public function flash() {
        return view('flash');
    }

    public function process() {
        //
        session()->flash('status', 'Task was successful!');

        return redirect()->route('shop.flash');
    }

    public function emptycart() {
        session()->forget('shopping_cart');
        return redirect()->route('shop.read');
    }

    public function add(Request $request)
    {
        session(['shopping_cart' => [
            '0123456789' => 6,
            '0987654321' => 3
        ]]);
    }


    public function dalja() {
        session()->flush();
        return redirect('/');
    }


    public function resp() {
       $user = User::find(1);

       return $user;
    }


    public function kuki() {
        return response('Hello World')->cookie(
            'numri', rand(1000,9999), 5
        );
    }

    public function kuki2() {
        $cookie = cookie('numri', 6765, 5);
        return response('Hello World')->cookie($cookie);
    }

    public function lexo() {

    }
}
