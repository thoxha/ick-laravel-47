<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TranslationsController extends Controller
{


    public function page()
    {
        $products = Product::all();
        $name = (Auth::check()) ? Auth::user()->name : "Visitor";

        return view('translations/page', compact('products', 'name'));
    }

    public function details($locale = "", $id)
    {
        $product = Product::find($id);
        return view('translations/details', compact('product'));
    }
}
