<?php

namespace App\Http\Controllers;

use App\Mail\OrderPlaced;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ProductsController extends Controller
{


    public function accessor($id, $qty)
    {
        $product = Product::find($id);


        return view('products.attribute', compact('product', 'qty'));
    }

    public function mutator($id, $price, $qty)
    {
        $product1 = Product::find(1);
        $product2 = Product::find(5);
        $product3 = Product::find(7);

        echo "<p>" . $product1->product_name;
        echo "<p>" . $product2->product_name;
        echo "<p>" . $product3->product_name;

        echo "<p>" . $product1::calculate(5, 3);
        echo "<p>" . Product::calculate(6, 4);


        die();

        $product = Product::find($id);
        dump($product);
        $product->new_price = $price;
        $product->total = ['price' => $price, 'quantity' => $qty];
//        $product->total = $price * $qty;
        dump($product);
//        return view('products.attribute', compact('product', 'qty'));
    }

    public function index()
    {
        $maincategories = Category::orderBy('category_name')->get();

        $products = Product::orderBy("product_name", "asc")->paginate(3);


        if ($products->isNotEmpty()) {
            return view('products/index', compact('products', 'maincategories'));
        } else {
            return view('products/create');
        }
    }


    public function myProducts()
    {
        return view('products/my_products');
    }


    public function byCategory(Category $category)
    {
        $maincategories = Category::orderBy('category_name')->get();
        $products = $category->products;
        return view('products/by_category', compact('category', 'products', 'maincategories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('products/create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'product_name' => 'required',
                'barcode' => 'required|max:13',
                'price' => 'required',
                'category_id' => 'required|exists:categories,id',
                'description' => 'required'
            ]
        );


        $product_name = filter_var($request->input('product_name'), FILTER_SANITIZE_STRING);
        $product_slug = Str::slug($product_name);
        $barcode = filter_var($request->input('barcode'), FILTER_SANITIZE_NUMBER_INT);
        $price = filter_var($request->input('price'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $category_id = (int)$request->input('category_id');

        $description = filter_var($request->input('description'), FILTER_SANITIZE_STRING);


        $product = new Product();
        $product->product_name = $product_name;
        $product->product_slug = $product_slug;
        $product->barcode = $barcode;
        $product->price = $price;
        $product->user_id = Auth::id();
        $product->category_id = $category_id;
        $product->description = $description;

        if ($product->save()) {
            echo "Produkti u shtua!";
        } else {
            echo "Gabim!";
        }

    }

    public function edit(Product $product)
    {
        Gate::authorize('update-product', $product);
        /*
                if ( Gate::denies('update-product', $product)) {
                   return view('report', ['report' => "Nuk ju lejohet!"]);
                }
        */

        $categories = Category::all();

        if ($product !== null) {
            return view("products/edit", compact('product', 'categories'));
        }

        echo "Nuk ekziston produkti me kete ID";
    }

    public function update(Request $request)
    {
        $product_id = filter_var($request->input('product_id'), FILTER_SANITIZE_NUMBER_INT);
        $product = Product::find($product_id);

        if (Gate::denies('update-product', $product)) {
            abort(403);
        }


        $product_name = filter_var($request->input('product_name'), FILTER_SANITIZE_STRING);
        $barcode = filter_var($request->input('barcode'), FILTER_SANITIZE_NUMBER_INT);
        $price = filter_var($request->input('price'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        $product = Product::find($product_id);
        $product->product_name = $product_name;
        $product->barcode = $barcode;
        $product->price = $price;

        if ($product->save()) {
            echo "Produkti u perditesua!";
        } else {
            echo "Gabim!";
        }

    }

    public function show(Product $product)
    {
        $product_title = $product->product_name;
        $lista = ["E hene", "E marte", "E merkure"];
        return view('products/show', compact('product', 'product_title', 'lista'));
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product !== null) {
            if (Gate::denies('delete-product', [$product, true])) {
                abort(403);
            } else {
                $product->delete();
                return redirect(route('products_index'));

            }
        }

        return "Produkti nuk u gjet!";
    }

    public function orderPlaced()
    {

        $image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAllBMVEX///8OfcIAesEAecAAdr8ihcWu0+oAd8AAfcKfx+QTgsSLwOHU6fS00+kjiMe82Oyry+XX5fKUweHd7fdOls3s9PpQotP4+/1Jm9Hy+PyhzejM3u5iptXu9fqt0emy1utantGFtds+kcuVv+CDuN1rqNW+1epjo9N9rtjk7vZ8tNs4jcm/3e/K5PI9lc1ssNqOut2lxOFTZAMOAAAPnElEQVR4nO1d6ZqquhLVBDcQZVBEQNRGlJbBab//y11QIGEUEdR9btav/oQOWVSlhqRIBgMKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKiv9TaDE+3Y8OoRmqpyjTqWmav4Isy/wdsvAb/mJOp4qnGp/uY0sY3sSUBHm3dQ66LSIOMjewEe5/Qg6Jtr5ytn/l3+NE+YeIaspxw1/dgx6MOJaFEAAwrEJ4DUIWcuL54F55afL16qtN/szcwzykVk+sjCoEKJgffP70tcJUj+OVHXIDz1HL8RyGmruaKZ8mU4C6uQTDIfcCtwxNbij6p09zSqCtPeEihmajC24ES8igi7n++LBUFXN/5mrYgZslCcckx6HRSByJIUYhEMfdf68TOmSC2VT9ID3PlB2bZUu7mFjI4GwdHHd73UUucLPZCBE2m9Ar7nbXrXuw5nZobyOm5S+IRc5m8iF6wu4gwhLhhdwYBtl65OUEyTwpXrWqGaoS+cy/V0cPQIUmQE7fvX9Irn+2VkivQA6yDGcf9vKvOVWeiVRCplNT9nXElGkEAPb2vRyP/nw0zPckFBw4X2amp66NluYhDO/MsR7KsshxGPhv8x/qOBjm6IVDjhktZqe2zDLQDOmCigoLOHH8BruqrQU9FFVGgcLQUvd/1t0+6LQPUEFLGFHqmeN6ehVJegAOR+Gg++nHnP/4c5TjCJiL18uz7lB+HY4wA4DlbOfaqyFXeGeUG5LQFvqKWU+8xUGCHtK3wql3X6xKPmKzYkTbXsR43J4xP8BwC958UzJnmD7KyBFwVveO4+gE6agHDKsvFfWN0aJhrrLxDgiEbp9wtFDyBMAy9sx7eyys/REzqgqGyw4bN8+J9QzzNnv8oaxN87Ougxl31LBhLmJ+AKD55dhRs23wY2cMDteJ9w8HOQtj8dnusk9P1ACeQ3pHgGavUzT34p0f5A5/Tx9PRAfGTCQpjpYvdkm52jf7AtiR//sdcyaaEJAUReml1mZz7s5PnE07DjlfgJmRov3Ciz/pd36MuPz8PAmJCekZod66HS+AN/Ny/umwc91gwhEUGb9tM144BIG4+qRzqIREWlSmbfzm2UzgfiW/0NzwHDEUzy2DY8/6Vn4hDJ/0/LOWjZhfu24QQrGIJHX+HY6sYwiEz+B2n+5NL3CJkTj/0Gxxv1CI2AbIn+7Nk1jeFzYiBKk31o4XfUEmbmPMEFpNRqJqYnw4g+BTjw7QJv5N4xe8eZw52L4bI6ynqEl4KiEmRVepZUvwKO366E/8m7Rfng8hSR+//T3O+dlrA8MviVitWzqYrlDC0JhJ0lldzrXZb3qbRwzEcwO1+26G6ngiBa51HcjEBNSCmNmcPm722xmeJJuHp8FfgqHEpPcx/ONmu2BoLDFecFFl41CWf+yB7hp7E9+n4vvgojrBS650wXDCYmv1gosqY6hcxjPNu/g7gonhYDVFlQxPidQ7YYhDfq5jhoOpM94s/V3GZsqEmlaZmmOwjf/qhOGwP4YDTeL5nOqb2JoyFR5REplr+ud3y7AMROTGlvdZEiFMGY5gAratx+9VhiXwcA4FL2U3RBNzKcPTZZWi7dTMu2WoungtbFFyXYpkDJNxmBa1vlDX+m4ZGlfM8Fy8LEU5JAi6nKp4twy1vzg0FQtXTRtG0+K1LTyLd8uQdBcof22q36ZDGwQ7T+DdMqxjqB4i+bKtZ1PL8YwMteq6m1YMudwlNyIIdaLKQFMw8CKFoVTAK0vISBnukjuLRCYCf926rrvdyceyZpozJMZhToazm/kZEVHswLTmKfD68ZH4NQvL2f/JL9cQMhwGyX25CQZ1eZiL95pLOESBVbKi3NzS7DDDUebKz20CAGYUKfT+IAbEHl9AoBIcCnKLsIQM7zWlYVujTOa2HgeIXFgJWwn2+XqV5t5ii0PvINMRO3oGdDItk1EbwZCYDCkCQJRZNJiU3CMSDLWNWKytBDC/CNjc46+wPyTXoNa3pAPYZubuNgwjGWyINjIyLDJUfabA794In9GF5nHpOW0PrIgXeV/WyE8Ut2MYDmZCAPUy9Cym5PoNkEz7mjOcEinwHv9s3gJyYOUSqpYMQ/XApqJWhp5eSTDsIOmYGzP8xYaGwboUJ8YoX2/TlmGoDOn7r5Oh6hRLYslWCFVoylC7Egxx6ijfVBfoeTfVmiE4p3pYJ8Nr7lquaJ2cD2zKcK0T84mpYzVGt19hIUF6zBDiSZiMRQTp9Bcpw+RuNI2bJ30Ew4jnM8oWOuOB1JShQswmnlOJXdj7D4XbHzP0/2xiLH2y5AP6iecnY5rLz+3WP/fCYY/QUQDOm9uPik+sVgM7dTxNGY6xkkI/YajcRztTzJkeMuQyYt8TPdYTBauOvGV8BYjYBExIPUsz1aYMxbJpmhWoEGEDGWYV28auSEysaWXkTbpmkQwSJoRHS1cBGzI8ErZZTF7y6d5hdlO8/0kZkiqCElNTKUMT93mYrQuViKXcpFcNGS6I5TUnGShO/BpLKpefleEfInFJGVbIUJulN8PVIAusp3BrPMOQNNxpiH26Gwj2WpKaPSvDH0JHHsmQMOswGysOBpu0nTR2aMbQIYZwkPTgGj89/5RGDHMyFMoYVsjQw03b+blpA7eTZHONGB5xf7H0J/O7nckHbM0YviBDvEiEB0wKbBHZ3+YMtQPhr1AytmOTzZZOzvQpQ2yVYHGxFhsMNk4xmjDMVEUdYpElkeGodAKxTxkSvsKRNzkQY9Q3mjKczkkRJiKbBrGSlhYuPCvDUoYVMiT9OpNH0eY/Zmi4ZAyYRhxyXM+8LZ3k6lOGQWniWwBYqA0Z8pkwPhHh+nJnyJXPkfYpw8cJyv25ekOGx0wYn9YmevefQVC+0vasDEstTYUMiYimC4aeTeoEjrHNe5eqKvn6lGHHDM9k1gXxolNssqFTPtfc5zgknHMdYCOG2oIkCNAaX4hFuy38TyuGz2gpqVWoGlwTS7NeZb/wwkmEFv8bqljCfo+3ANasBpvH/tBzSH5DcMEa6cU9GpVkTo0YvqCll1Sv2CbFrjUMlSxBSEz0pbGhWLEk2qcMiajt0qDErprhycqqaMYvzNj4x4pipT7HIY68wbnBR7WVDDe50AFl5spjTSmmL00ZviBD77kSuyqG4/xn3X7GLcSmFNgVXzD1OQ5VPBtTXhXShCE5a3XvppP9t/ghYF6x9Npx9pQpvSYnp9niJy7eMfvWyxiueZT/Zj0/HxIMX2TYQIYK8fzMTIlJTibmh8nEYa8ZimUVtH5+MxBwyNus0asMG4xDg+iATcrKs7AAoJ6ZRNEEHeJpiBtk/NC0vvSQX/VwCiYLJQwrzPUR26nWMiTnaIElnyYRbm9UJlNye5d+japK7m3HBOgS3SJedspwlWEIOL9ok9PmK8z16fyAYQMZDsh+AGTjdXzFIq9EW03wyyV/da34I9yQIu7XBCfw5QwBupbYy+RilT8k9KC1PyTrkof3lfxkHV9G2SvRflGIIxegCLU71DMEqPTz3+QJQdXnetcOtHSQ91jp6prmPMrzwSGNwJbp6yhjCMVyISVdrYpLiYHYXksHm8IyaLpCaj2gSJTYaamaFhkCtKqwlbG3qMwtBppbz7CJlg60VZ5iusqtzGsXgYFOBMzH5KuZPEPAzaskhDOYivwwKUKpZNhIhgNFz/HAlQpKjRQBdDKqx8fqnmMIxG31F05J4ggqcvxBNMEDaxg2GoeDwTTHg6g28bb5uCQBHO1yJl62by4+wxByjlSTmvhx43Uf68U7fLwiw5DHNbNfDlkxZEgWKOEIh86x8Nqne5GBgGAI4cKsTUz45LFizcYuHh81SzJESS0YBPlxmBaJwWy+YEz3iGGSonGUuahKCzZbNgSZ4epYlg1o3mYlJmuKqsWu6vkRrgrUptnrn4vI4KKB42qRIutmTjq+UhgbmrLcX25Y5bet0tSxzsWb0EaFDItN9U4+uCLb+CmpccxBTRjCw8PvvHr/xnl9+lkul5tu95rVUi9aNY/xryOdTcwVkf2HkE4H4bWa/xhwcQboeJusb4GXujagf/N2Ce2hEsvJ/00haqnPD+Pz/6atMXEERvj0r4fx2NcnIOsDua/ZJvsBNNNfNN978i8xp6d/cvvo5pjsAuaJrhKFgGGa+A8MRWU3DyP7Z4SxJRiOvn4/EPW+m9pTDI/kjicvbubWN9ZjmwPPD6hMoY1YVsD3JVj7eDfRpxhOxH+BojG9sHia4EmjOM5OHX+jonrSapjp5XMMjcwqKkDCl1lUw/yrD7NTOc86Nim7woFm3+QXlY0bFPdhf9Z177NLC8hpsOz8FmjS1hqVnbLwLEMvP2VrV04ivxPKeD7iSmeMnw+/Mvt/Ri2A1ac1VV2eh4UN9NszHAijXGOQ5T9ncAxvqcPyLy9bM9T4Qq0gexY+IUfNm8oLUEevHcOBcS02MzwIb95f0ZhKuwV6QG+Y+6a+Kdb74uoB5Kz8Bjc9wjvK7hmVH7+T79e+zaySkfUZcVvD+bZubacraCfetQKu0clQABxajp9SiqGuita4X0FOls48+iS/UW07YM5S602otVnFCU1DdO5r4/zTeCUiLv99bA0/8bVdmgWx4kmAZdBFULs7YFLTDGV5CRiGbX5mWfim9ZfXV05W9QNCD3X2hVcPlowOrvRM+aKjJw9kA1zgdLF+pGzL46T4KSFLe3UVnjyW6k5t7U1N85ffr87PkoteL+rsfC5NDtjaZ92O3wrio8VOk3qq2lpVJqejJMh/r+5KDxDLlJ1X9gAAiq7coSEw3cp4ED8zOh4OoMDWrYMbHQ+34/nlMv0ya8nz/Gx33W5dJzohLhDRkIWVh8Q9AIQ6/2g1+0msBbtejCTT2xF/gOM4hEYYCHH4kL9XznYE0VmHPZzvou6HtaU8bwJgoF6zpP8avEtjH9UTu8g3LHuN/U8r8fF47I0eCg5vOFP16Nof4BiOXnHuvrKb6xPQTjvrvQMSsEPb2b01aVMER3ycr3XDDjLIugqnt9dMqFNef5Rzd8EuCgmnpVu+9Y8wSJ7ZTG8kw6ieCS5/ao5dfQ9UXkfDbg5mxtyi5pC+L91l7xPQjnvrfrh2B9QAGgWB5X/qSN9qKMLMtWyRaxlk3mI8JNrzgzvbfPFJ6Sch+iwi5Mk8OOIeiyzaijiipjvulReOzWspPgdDOYU50c5f6WcR3XZFYFmYxa1YlAEoOqA6JHY7yHnyzjMgu4BmeMo0TGzNX5mfXff+5eI4zspxLhffj07cln/Da9OpEp12/Om+voyoeNcg8cr+yhQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/xz+B682O3QuahVBAAAAAElFTkSuQmCC';

        $order = (object) [
            'date' => '12.06.2021',
            'total' => 8,
            'image' => $image,
            'items' => [
                ['name' => 'Cokolade', 'quantity' => 3, 'price' => 1],
                ['name' => 'Chips', 'quantity' => 2, 'price' => 1.5],
                ['name' => 'Kikirika', 'quantity' => 1, 'price' => 2],
            ],
        ];

        $client = User::find(1);

        Mail::to($client)->send(new OrderPlaced($order, $client));
    }
}
