<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class BaseViewComposer
{

    public function compose(View $view)
    {
        $data = $view->getData();

        $data['layout_categories'] = Category::all();
        $data['total_products'] = Product::count();
        $view->with($data);
    }
}
