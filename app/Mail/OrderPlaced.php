<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $order;
    protected $client;

    public function __construct($order, $client)
    {
        $this->order = $order;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['Akademia Virtuale' => 'laravel@akademiavirtuale.com'])
            ->view('emails.order')
            ->text('emails.order_text')
            ->attach('books/flutter.pdf', [
                'as' => 'libri2.pdf',
                'mime' => 'application/pdf',
            ])
            ->attachFromStorage('public/lista2.csv')
            ->attachFromStorageDisk('products', 'lista.csv')
            ->with(
                [
                'orderDate' => $this->order->date,
                'imageData' => $this->order->image,
                'orderTotal' => $this->order->total,
                'orderItems' => $this->order->items,
                'clientName' => $this->client->name
            ]);
    }
}
