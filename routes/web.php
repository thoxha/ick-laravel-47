<?php

use App\Http\Controllers\CacheController;
use App\Http\Controllers\FilesController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\TranslationsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['admin', 'auth'])->name('dashboard');


Route::prefix("students")->name('students.')->group(function () {
    Route::get('/', [StudentsController::class, 'index']);
    Route::get('/login', [StudentsController::class, 'authenticate'])->name('login');
    Route::get('/logout', [StudentsController::class, 'logout'])->name('logout');
    Route::get('/reg', [StudentsController::class, 'create']);
    Route::post('/store', [StudentsController::class, 'store']);
    Route::post('/attach', [StudentsController::class, 'attach'])->name('students.attach');
    Route::get('/detach/{student}/{subject}', [StudentsController::class, 'detach'])->name('students.detach');
    Route::get('/notat/{student}', [StudentsController::class, 'notat'])->name('students.notat');
});

Route::get('country_villages/{country}', [StudentsController::class, 'countryVillages'])->name('country.villages');

Route::prefix("products")->group(function () {
    Route::get('/', [ProductsController::class, "index"])->name('products_index');
    Route::get('/create', [ProductsController::class, "create"])->name('products_create');
    Route::post('/store', [ProductsController::class, "store"])->name('products_store')->middleware(['auth']);
    Route::get('/show/{product}', [ProductsController::class, "show"])->name('products_show');
    Route::get('/edit/{product}', [ProductsController::class, "edit"])->name('products_edit');
    Route::post('/update', [ProductsController::class, "update"])->name('products_update');
    Route::get('/destroy/{id}', [ProductsController::class, "destroy"])->name('products_destroy');
    Route::get('/by_category/{category}', [ProductsController::class, "byCategory"])->name('products_by_category');
    Route::get('/my_products', [ProductsController::class, "myProducts"])->name('my_products')->middleware(['auth']);
    Route::get('/accessor/{id}/{qty}', [ProductsController::class, "accessor"])->name('accessor');
    Route::get('/mutator/{id}/{price}/{qty}', [ProductsController::class, "mutator"])->name('mutator')->where('price', '[0-9]+');
    Route::get('/place_order', [ProductsController::class, 'orderPlaced'])->name('place_order');
});

Route::prefix("files")->group(function () {

    Route::get('ruaje', [FilesController::class, 'ruaje'])->name('ruaje');
    Route::get('db2csv', [FilesController::class, 'db2csv'])->name('db2csv');
    Route::get('lexoje', [FilesController::class, 'lexoje'])->name('lexoje');
    Route::get('linku', [FilesController::class, 'linku'])->name('linku')->middleware(['auth']);
    Route::get('url', [FilesController::class, 'url'])->name('url');
    Route::get('kopjo', [FilesController::class, 'kopjo'])->name('kopjo');
    Route::get('form', [FilesController::class, 'form'])->name('form')->middleware('auth');
    Route::post('submit_form', [FilesController::class, 'submitForm'])->name('files.submit_form')->middleware('auth');
    Route::get('product/{id}', [FilesController::class, 'product'])->name('files.product');
    Route::get('delete/{id}', [FilesController::class, 'delete'])->name('files.delete');
});


Route::prefix('shop')->group(function() {
    Route::get('add', [ShopController::class, 'add'])->name('shop.add');
    Route::get('read', [ShopController::class, 'read'])->name('shop.read');
    Route::get('push', [ShopController::class, 'push'])->name('shop.push');
    Route::get('pull', [ShopController::class, 'pull'])->name('shop.pull');
    Route::get('inc', [ShopController::class, 'inc'])->name('shop.inc');
    Route::get('process', [ShopController::class, 'process'])->name('shop.process');
    Route::get('flash', [ShopController::class, 'flash'])->name('shop.flash');
    Route::get('emptycart', [ShopController::class, 'emptycart'])->name('shop.emptycart');
    Route::get('dalja', [ShopController::class, 'dalja'])->name('shop.dalja');
    Route::get('resp', [ShopController::class, 'resp'])->name('shop.resp');
    Route::get('kuki', [ShopController::class, 'kuki'])->name('shop.kuki');
    Route::get('kuki2', [ShopController::class, 'kuki2'])->name('shop.kuki2');
});


Route::prefix('cache')->group(function() {
    Route::get('put', [CacheController::class, 'ruaje'])->name('cache.put');
    Route::get('get', [CacheController::class, 'lexoje'])->name('cache.get');
    Route::get('query', [CacheController::class, 'query'])->name('cache.query');
});

Route::get('mirembajtja/{id}',[CacheController::class, 'mirembajtja']);

Route::get('/test', [StudentsController::class, "test"]);
Route::get('/db', [StudentsController::class, "db"]);

Route::get('/report', function () {
    return view('annual_report');
})->middleware('auth.basic');


Route::get('{lang}/page', [TranslationsController::class, 'page'])->name('translations.page');
Route::get('{lang}/details/{id}', [TranslationsController::class, 'details'])->name('translations.details');

require __DIR__ . '/auth.php';
