<?php


return [

    'default_currency' => env('DEFAULT_CURRENCY','$'),
    'currencies' => [
        'USD' => '$',
        'EUR' => '€'
    ],
    'languages' => ['en', 'sq', 'de']


];
